# Projet Tweetoscope 2021 groupe 5
Eliott Bigiaoui, Alexandre Bringolf, Lucien Charbonnel
##  Comment déployer le projet ?

### 1 - Lancement en dur dans un terminal

Prérequis : 
- Librairie [gaml](https://github.com/HerveFrezza-Buet/gaml)
- [cppkafka](https://github.com/mfontanini/cppkafka)

Il suffit de git clone le projet avec la commande `git clone https://gitlab-student.centralesupelec.fr/2019bringolfa/tweetoscope_2021_05.git` et de lancer les fichiers ci dessous :

- Le générateur avec la commande `g++ -o tweet-generator -O3 -Wall -std=c++17 tweet-generator.cpp pkg-config --libs --cflags gaml cppkafka -lpthread`
- Le collecteur avec la commande `python3 collector.py --broker-list localhost:9092 --obs-window --termination-time`
- L'estimateur avec la commande `python3 estimator.py --broker-list localhost:9092`
- Le prédicteur avec la commande `python3 collector.py --broker-list localhost:9092 --obs-window`
- Le learner avec la commande `python3 collector.py --broker-list localhost:9092`

### 2 - Création des containers Docker

Afin de construire les container Docker, il suffit de se placer dans les dossiers des modules respectifs et de lancer la commande suivante : 
`docker build -f Dockerfile.<nom du module> .`

### 3 - Déploiement kubernetes

Enfin, afin de le déployer sur kubernetes, il faut avoir minikube installé et exécuter les commandes suivantes : 
- `minikube start`
- `kubectl apply -f zookeeper-kafka.yml`
- `kubectl apply -f tweetoscope.yml`

## Vidéo explicative

[Lien google drive](https://drive.google.com/drive/folders/1ysqA3YAoCxt_M2p3Z8pv-uKeDCs326SY?usp=sharing)


