#External packages

from kafka import KafkaConsumer, KafkaProducer
import json 
import argparse 
import numpy as np
import pickle
import logger

#Définition des fonctions utiles pour les randoms forests


def w_params(params, alpha=2.4, mu=10):
   p,beta,G1=params
   n_star=p*mu*(alpha-1)/(alpha-2)
   return [beta,n_star,G1]

def w_Tobs (n_true, n_obs, params, alpha=2.4, mu=10):
   p,beta,G1=params
   n_star=p*mu*(alpha-1)/(alpha-2)
   res=(n_true-n_obs)*(1.-n_star)/G1
   return res
   
def n_predicted(n_Tobs, w_Tobs, params, alpha=2.4, mu=10):
   p,beta,G1=params
   n_star=p*mu*(alpha-1)/(alpha-2)
   return int( n_Tobs+w_Tobs*G1/(1.-n_star) )



if __name__ == '__main__':
   parser = argparse.ArgumentParser(formatter_class = argparse.RawTextHelpFormatter)
   parser.add_argument('--broker-list', type=str,required=True,help="The broker list")
   parser.add_argument('--obs-window', type=str,required=True,help="The observation window")
   args = parser.parse_args()

   logger = logger.get_logger('{}s window prediction'.format(args.obs_window), broker_list=args.broker_list, debug=True) 

   #Kafka Consumers
   consumer_properties = KafkaConsumer('cascade_properties',          
         bootstrap_servers = args.broker_list,                        
         value_deserializer=lambda v: json.loads(v.decode('utf-8')),  
         key_deserializer= lambda v: v.decode(),                      
        
   )

   #Création d'un topic spécifique pour le learner pour plus de clarté 
   consumer_models = KafkaConsumer('models',                          
         bootstrap_servers = args.broker_list,                        
         value_deserializer=lambda v: pickle.loads(v),                
         key_deserializer= lambda v: v.decode()
   )

   #Kafka Producers
   producer_alerts = KafkaProducer(
         bootstrap_servers = args.broker_list,                     
         value_serializer=lambda v: json.dumps(v).encode('utf-8'),
         key_serializer=str.encode                                
   )
   producer_samples = KafkaProducer(
         bootstrap_servers = args.broker_list,                     
         value_serializer=lambda v: json.dumps(v).encode('utf-8'), 
         key_serializer=str.encode                                 
   )
   
   partial_cascades={} #dictionnaire qui stocke les cascade partielles par identifiants avec [données de type parameters(nb de tweets obs sur la periode, nb de tweets estimés par l'estimateur MAP, les paramètres du modèle)] 
   reg_test=False 
   ARE_m = []
   
   while True:
        lecture = consumer_models.poll(1000)
        for topicPartition, consumerRecords in lecture.items():
            for msg in consumerRecords:
                if msg.key == args.obs_window:
                    reg = msg.value
                    reg_test=True

       #On observe en premier lieu des cascades partielles en envoyant une alerte si on estime qu'elle sera potentiellement grande
        
        lecture = consumer_properties.poll(1000)
        while lecture is None: 
            continue
        for topicPartition, consumerRecords in lecture.items():
            for msg in consumerRecords:

                ###### Envoi d'alertes ######

                if msg.key == args.obs_window:
                        if msg.value['type']=='parameters':
                            logger.debug("Nouveau tweet : n°{}".format(msg.value["cid"]))
                            n_predict = 0 #Valeur par défaut, pas de prédiction faite
                            if reg_test:
                                w_predicted=reg.predict([w_params(msg.value['params'])])[0]
                                n_predict=n_predicted(msg.value['n_obs'],w_predicted,msg.value['params'])
                                msg_alerts = {'type': 'alert', 'cid': msg.value['cid'], 'T_obs' : msg.key, 'n_predicted': n_predict}
                                producer_alerts.send('alerts', key = "None", value = msg_alerts)
                                logger.debug("La cascade tweet n°{} devrait générer {} retweets".format(msg.value['cid'],msg_alerts['n_predicted']))
                            partial_cascades[msg.value["cid"]]=[msg.value,n_predict]

                ###### Génération de samples ######

                if msg.value['type']=='size':
                        if msg.value["cid"] in partial_cascades:
                            #On enlève la cascade partielle des cascades à l'étude car elle est terminée et on s'en sert pour notre apprentissage
                            partial_cascade=partial_cascades.pop(msg.value["cid"])
                            logger.debug("La cascade tweet n°{} est terminée, elle a généré {} tweets".format(msg.value["cid"],msg.value["n_tot"]))
                            X=w_params(partial_cascade[0]['params']) 
                            W=w_Tobs(msg.value['n_tot'],partial_cascade[0]['n_obs'],partial_cascade[0]['params'])
                            msg_samples = {'type': 'sample', 'cid': msg.value['cid'], 'X' : X, 'W' : W}
                            producer_samples.send('samples', key = args.obs_window, value = msg_samples)
                            
                            #### Calcul de l'ARE  ####
                            ARE = (abs(partial_cascade[1]-msg.value["n_tot"])/msg.value["n_tot"])
                            ARE_m.append(ARE)
                            msg_stats = {'type': 'stat',  
                                        'cid': msg.value['cid'],    
                                        'T_obs' : msg.key,
                                        'ARE' : "{}".format(ARE),
                                        'ARE_m': "{}".format(sum(ARE_m)/len(ARE_m))
                                        }
                            producer_alerts.send('stats', key = "None", value = msg_stats)
    
        
    

            

                
                


