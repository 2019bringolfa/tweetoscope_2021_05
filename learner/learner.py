from kafka import KafkaConsumer, KafkaProducer
import json 
import argparse 
import numpy as np
import pickle

import logger

from sklearn.ensemble import RandomForestRegressor
from sklearn.datasets import make_regression

if __name__ == '__main__':
    parser = argparse.ArgumentParser(formatter_class = argparse.RawTextHelpFormatter)
    parser.add_argument('--broker-list', type=str,required=True,help="The broker list")
    args = parser.parse_args()

    logger = logger.get_logger('learner-node', broker_list=args.broker_list, debug=True) 

    #Kafka Consumers
    consumer_samples = KafkaConsumer('samples',                        
            bootstrap_servers = args.broker_list,                        
            value_deserializer=lambda v: json.loads(v.decode('utf-8')),  
            key_deserializer= lambda v: v.decode()                       
    )

    #Kafka Producers
    producer_models = KafkaProducer(
            bootstrap_servers = args.broker_list,                     
            value_serializer=lambda v: pickle.dumps(v),                
            key_serializer=str.encode                                 
    )

    learned={} # dictionnaire contenant les infos à renvoyer dans le topic 
    for msg in consumer_samples:
        #Si un échantillon a déjà été étudié sur cette fenetre de temps, on ajoute les données d'apprentissage à notre régression
        if msg.key in learned:
            X,w=learned.get(msg.key)[0]
            reg=learned.get(msg.key)[1]
            X.append(msg.value['X'])
            w.append(msg.value['W'])
            reg.fit(X,w)
            producer_models.send('models', key = msg.key, value = reg) 
            learned[msg.key]=((X,w),reg)
        #Sinon on construit la régression
        else:
            X=[msg.value['X']]
            w=[msg.value['W']]
            reg = RandomForestRegressor(n_estimators=10)
            learned[msg.key]=((X,w),reg)



