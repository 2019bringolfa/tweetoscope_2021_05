#External packages
from logging import debug
from kafka import KafkaConsumer, KafkaProducer
import json 
import sys
import ast 
import argparse 
import numpy as np
import logger

#Internal packages

from hawkes_process import compute_MAP, prediction


if __name__ == '__main__':
   parser = argparse.ArgumentParser(formatter_class = argparse.RawTextHelpFormatter)
   parser.add_argument('--broker-list', type=str,required=True,help="The broker list")
   args = parser.parse_args()

   logger = logger.get_logger('Lancement du processus de Hawkes', broker_list=args.broker_list, debug=True) 

#On récupère les données envoyées sur le topic cascade_series : notre estimateur se base sur les cascades partielles 
   consumer = KafkaConsumer('cascade_series', bootstrap_servers=args.broker_list,
   value_deserializer=lambda v: json.loads(v.decode('utf-8')),  
   key_deserializer= lambda v: v.decode()                      
   )

   producer = KafkaProducer(bootstrap_servers=args.broker_list)

   for message in consumer:
      msg=message.value
      map = compute_MAP(np.array(msg['tweets']),msg['tweets'][-1][0])
      p, beta = map[1]
      n_tot,G1 = prediction((p, beta), np.array(msg['tweets']), msg['tweets'][-1][0])
      estim = {'type': 'parameters',  
                     'cid': msg['cid'],       
                     'n_obs': len(np.array(msg['tweets'])),          
                     'n_supp' : int(n_tot),  
                     'params': [p,beta,G1] }
      producer.send('cascade_properties', key = b'%d' % msg['T_obs'], value = json.dumps(estim).encode('utf-8'))
      producer.flush()

