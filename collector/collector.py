import argparse                
import json                       
from kafka import KafkaConsumer   
from kafka import KafkaProducer
import time

parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter)
parser.add_argument('--broker-list', type=str, required=True, help="the broker list")
parser.add_argument('--obs-window', type=int,nargs='+', required=True, help="the list of window observations (in seconds)")
parser.add_argument('--termination-time', type=int, required=True, help="time (in seconds) between 2 tweets to consider a cascade terminated")
args = parser.parse_args() 

consumer = KafkaConsumer('tweets',                   
  bootstrap_servers = args.broker_list,                        
  value_deserializer=lambda v: json.loads(v.decode('utf-8')), 
  key_deserializer= lambda v: v.decode()                      
)

producer = KafkaProducer(
  bootstrap_servers = args.broker_list,
  value_serializer=lambda v: json.dumps(v).encode('utf-8'),
  key_serializer= str.encode 
)

messages_series = {}
sent_properties=[]
window=args.obs_window


#Cette fonction teste si notre cascade a dépassé la fenêtre d'observation
def partial_cascades(msg,seconds):
  if (messages_series[msg.key]['T_obs']<=seconds+messages_series[msg.key]['tweets'][0][0]) and (msg.value['t']>seconds+messages_series[msg.key]['tweets'][0][0]):
    messages_series[msg.key]['T_obs'] = seconds
    producer.send('cascade_series',key='None',value=messages_series[msg.key])


for msg in consumer:

  # On initialise une cascade à l'arrivée d'un nouveau tweet
    if msg.value['type'] == 'tweet':
      messages_series[msg.key] = {'type':'serie', 'cid':msg.key,'T_obs':msg.value['t'],'tweets':[(msg.value['t'],msg.value['m'])]}

    else:
      #On teste la condition d'arrêt de la cascade sur la fin de la fenêtre d'observation
      if msg.value['t'] - messages_series[msg.key]['T_obs'] < args.termination_time:
        for observation in window: 
          partial_cascades(msg,observation)
        messages_series[msg.key]['T_obs'] = msg.value['t']
        messages_series[msg.key]['tweets'].append((msg.value['t'],msg.value['m']))
      else:
        #On envoie le message sur notre Topic uniquement s'il n'a pas déjà été envoyé. 
        if msg.key in sent_properties:
          pass
        else:
          dict = {'type':'size','cid':messages_series[msg.key]['cid'],'n_tot':len(messages_series[msg.key]['tweets']),'t_end':messages_series[msg.key]['T_obs']}
          for t in window:
            producer.send('cascade_properties',key=str(t),value=dict)
          sent_properties.append(msg.key)

